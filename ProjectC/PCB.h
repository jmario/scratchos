#ifndef PCB_H
#define PCB_H

	struct PCB {
		unsigned int status;
		unsigned int sp;
		unsigned int segment;
		struct PCB *waiter;
	};
	
	struct regs {
		unsigned int es;
		unsigned int ds; 
		unsigned int ax;
		unsigned int bp;
		unsigned int di;
		unsigned int si;
		unsigned int dx;
		unsigned int cx;
		unsigned int bx;

		unsigned int ip;
		unsigned int cs;
		unsigned int flags;
	};

int getFreeSegment();
void InitProcessQueue();
void kill(int index);
//int getCurrentProcessIndex();
//struct PCB[] getProcessQueue();

#endif
