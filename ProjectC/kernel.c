//#include "PCB.h"
#define BASE_ADDRESS 0xB000
#define RMVA 160
#define memVideoAddr 0x8000
#define lineLength 80

void printString(char* str);
void printColoredString(char* str, int color);
void readString(char* str, int color);
void readFile(char* filename, char* buffer);
int fileExists(char* filename, char* buffer);
void cleanBuffer(char* buffer, int size);
void nextLine();
void drawBar();
void execute(char* prg);
void listFiles();
void terminate();
void printChar(char ch, int color);
int deleteFile(char* filename);
int writeFile(char* name, char* buffer, int size);
int strLen(char* buffer);
int copyFile(char* sourceFile, char* sourceBuffer char* destinyFile);
int getFileSize(char* filename);
void printInt(int number, int color);
void itoa(int number, int color);
int readString2(char* str, int color);
void initializeProgram(int segment);
void killProcess(int index);
int getFreeSegment();
void InitProcessQueue();
void Kill(int index);
int getCurrentProcessIndex();
void copyProcessQueue();
void listProcess();

	struct PCB {
		unsigned int status;
		unsigned int sp;
		unsigned int segment;
		struct PCB *waiter;
	};
	
	struct regs {
		unsigned int es;
		unsigned int ds; 
		unsigned int ax;
		unsigned int bp;
		unsigned int di;
		unsigned int si;
		unsigned int dx;
		unsigned int cx;
		unsigned int bx;

		unsigned int ip;
		unsigned int cs;
		unsigned int flags;
	};

struct PCB process_queue[8];
struct PCB copy_process_queue[8];
struct PCB *currentProcess = 0;

void main(){
	int i=0;
	clrScreen();
	setKernelDataSegment();
	InitProcessQueue();
	restoreDataSegment();
		
	for(; i<8; i++){
		itoa(process_queue[i].segment,0x7);
		printString("   ");
		itoa(process_queue[i].status, 0x6);
	}
		
	makeInterrupt21();
	execute("shelly");
	irqInstallHandler();
	setTimerPhase(100);


}

void InitProcessQueue(){
	int i = 0;
	int segment = 0x2000;  	

	for( ; i<8; i++){
		process_queue[i].status = 4; 
		process_queue[i].sp = 0xff00; 
		process_queue[i].segment = segment; 
		process_queue[i].waiter = 0;
		segment += 0x1000;
	}

}

int getFreeSegment(){
	int i = 0;	
	for( ; i < 8; i++){
		if(process_queue[i].status == 4){
			process_queue[i].status = 1;
			return process_queue[i].segment;		
		}
	}
	return -1;
}

void printChar(char ch, int color){
	int row, column;
	row = getCurrentRow();
	column = getCurrentColumn();
	if(row == 24){
		scrollUp(1);
		moveCursor(0, row-1);
		if(ch!='\t' && ch!= '\r' && ch!= '\n')
			printColoredChar(ch, color);	
		putInMemory(BASE_ADDRESS, memVideoAddr+(159)+(24*RMVA), 0x77);
	}else if(ch == '\t'){
		moveCursor(column+2, row);
	}else if(ch == '\n'){
		moveCursor(0, row+1);
	}else if(ch == '\r'){
		moveCursor(0, row);
	}else if(ch == 0x8){
		moveCursor(column-1, row);
		printColoredChar(0x0, color);
		moveCursor(column-1, row);
	}else{
		printColoredChar(ch, color);	
	}
	
}

void printString(char* str){
	printColoredString(str, 0x7);
}

void printColoredString(char* str, int color){
	int i;	
	for(i=0; str[i] != '\0'; i++)
		printChar(str[i], color);
}

void readString(char* str, int color){
	int i = 0;
	char current = 0;
	cleanBuffer(str, lineLength);
	
	while( current != 0xD ){
		current = readChar();
		if(current == 0x8 && i > 0){
			printChar(current, 0x7);
			i--;	
		}else if(i < lineLength-1 && current != 0x8 && current != 0xD){
			str[i] = current;
			printChar(str[i], color);
			i++;
		}
	}
	nextLine();
	str[lineLength] = 0;
}

void readFile(char* filename, char* buffer){

	int fileIdx, i, j;
	int bufferAdr = 0;
	int sectors[26];

	readSector(buffer, 2);
	fileIdx = fileExists(filename, buffer);
	if (fileIdx != -1){

		for(j=0;j<26;j++)
			sectors[j] = buffer[fileIdx+j];

		for(i=0; sectors[i] != 0 && i<26; i++){
			readSector(buffer+bufferAdr, sectors[i]);
			bufferAdr += 512;
		}
	}
	else{
		buffer[0]=='\0';
	}
}

int fileExists(char* filename, char* buffer){
	
	int i, j, filesize, exists = 0;
	for (i = 0; i < 16; i++)
		if (buffer[32*i] != 0){
			for (j=0; j < 6; j++)
				if (buffer[j+(32*i)] == filename[j]){
					exists = 1;
				}else{
					exists = 0;
					break;				
				}
	
			if (exists)
				return (i*32)+6;
		}

	if (!exists){
		filesize = strLen(buffer);
		cleanBuffer(buffer, filesize);
	}
	return -1;
}

void listFiles(){
	char buffer[13312];
	char filename[7];
	int i, j, size;	
	filename[6] = 0x00;	
	readSector(buffer, 2);
	for (i = 0; i < 16; i++)
		if (buffer[32*i] != 0){
			for (j=0; j < 6; j++){
				printChar(buffer[(i*32)+j],i+9);
				filename[j] = buffer[(i*32)+j];		
			}
			printChar('\t', 0x0);	printChar('\t', 0x0);	printChar('\t', 0x0);
			size = getFileSize(filename);
			itoa(size, i+9);
			printChar('\t', 0x0);	printChar('\t', 0x0);	printChar('\t', 0x0);
			itoa(size*512, i+9);
			nextLine();
			cleanBuffer(filename, 6);
		}
}

void nextLine(){
	printChar('\r', 0x0);
	printChar('\n', 0x0);
}

void cleanBuffer(char* buffer, int size){
	int i = 0;	
	for(; i<size; i++)
		buffer[i] = 0x0;
}

void drawBar(){
	char label[29];
	int x, y;
	for(y=0; y < 25; y++){
		for(x=0; x < RMVA; x++){
			if(y<2){
				putInMemory(BASE_ADDRESS, memVideoAddr+(x)+(y*RMVA), 0x33);
			}
			if(x==RMVA-1 && y>=2){
				putInMemory(BASE_ADDRESS, memVideoAddr+(x)+(y*RMVA), 0x77);
			}

		}
	}
	
	label[0] = ' '; label[1] = ' '; label[2] = 'J'; label[3] = 'M'; label[4] = 'I'; label[5] = 'B'; label[6] = 'A'; label[7] = 'R'; 
	label[8] = 'R'; label[9] = 'A'; label[10] = ' '; label[11] = 'V'; label[12] = 'e'; label[13] = 'r'; label[14] = 's'; label[15] = 'i';
	label[16] = 'o'; label[17] = 'n'; label[18] = '['; label[19] = '3'; label[20] = '.'; label[21] = '3'; label[22] = '.'; label[23] = '6';
	label[24] = '4'; label[25] = ']'; label[26] = ' '; label[27] = ' '; label[28] = 0;

	moveCursor(28, 1);
	printColoredString(label, 0x4F);
	moveCursor(0, 4);
}

void execute(char* prg){
	char buffer[13312];
	int seg;
	
	readFile(prg, buffer);
	setKernelDataSegment();
	seg = getFreeSegment();
	restoreDataSegment();

	itoa(seg, 0xF);
	if(seg != -1){
		copyToSegment(seg, 0, buffer, 13312);
		//launchProgram(seg);
		initializeProgram(seg);
	}
	/*setKernelDataSegment();
	currentProcess->segment = seg;
	restoreDataSegment();*/
}

void initializeProgram(int segment){

	struct regs context;
	context.es = segment; 	//es
	context.ds = segment; 	//ds 
	context.ax = 0;	      	//ax
	context.bp = 0;	 	//bp
	context.di = 0;	 	//di
	context.si = 0;		//si
	context.dx = 0;	 	//dx
	context.cx = 0;  	//cx
	context.bx = 0;  	//bx
	
	context.ip = 0;	 	//ip
	context.cs = segment; 	//cs
	context.flags =	0x0200; //flags

	copyToSegment(segment, 0xff00, &context, 24);
	 
}

void terminate(){
	/*char shell[6];
	shell[0] = 's'; shell[1] = 'h'; shell[2] = 'e'; shell[3] = 'l'; shell[4] = 'l'; shell[5] = 'y';
	execute(shell);*/
	currentProcess->status = 4;
	#asm
	sti
	#endasm
	while(1);
}

int deleteFile(char* filename){
	int fileIdx, i, j, cleanedSectors = 0;
	char map[512];
	char dir[512];
	int sectors[27];
	readSector(dir, 2);
	readSector(map, 1);
	
	fileIdx = fileExists(filename, dir)-6;
	if (fileIdx != -7){
		
		for(i = 0; i < 6; i++)		
			dir[fileIdx + i] = 0x00;

		for(j = 0; j < 26; j++){
			sectors[j] = dir[fileIdx+i+j];
			dir[fileIdx+i+j] = 0x00;
			cleanedSectors++;
		}
		sectors[26] = 0x00;
		for(i = 0; i < cleanedSectors; i++){
			if(sectors[i] == 0x00)
				break;
			map[sectors[i]] = 0x00;
		}

	writeSector(dir, 2);
	writeSector(map, 1);
	
	return 1;
	}

	return 0;
}

int writeFile(char* filename, char* buffer, int size){
	int fileIdx, i, j = 0;
	int sectorsNeeded;
	int sectorOccupied = 0;
	char map[512];
	char dir[512];
	char newFileContent[512];
	readSector(dir, 2);
	readSector(map, 1);

	if(mod(size, 512) > 0)
		sectorsNeeded = (size/512) + 1;
	else 
		sectorsNeeded = size/512;	

	if(fileExists(filename, dir) == -1){
		readSector(dir, 2);
		for (i = 0; i < 16; i++)
			if (dir[32*i] == 0x00)
				break;
			
		fileIdx = i*32;
		//if(i != 16){
			for(i = 0; i < 6; i++)
				dir[fileIdx+i] = filename[i];
			
			for(j = 0; j < sectorsNeeded; j++){
				for(sectorOccupied = 0; map[sectorOccupied] != 0x00; sectorOccupied++){
					
				}
				
				/*if(sectorOccupied == 26)
					return -1;
*/

				map[sectorOccupied] = 0xFF;
				dir[fileIdx+6+j] = sectorOccupied;
				cleanBuffer(newFileContent, 512);

				for(i=0;i<512;i++){
					/*if (buffer[i+(j*512)]==0x0){	break;	}*/
					newFileContent[i] = buffer[i+(j*512)];
				}

				writeSector(newFileContent, sectorOccupied);
			}

			writeSector(dir, 2);
			writeSector(map, 1);
		/*}else{
			return -2; //print ERROR ran out of sectors
		}*/
			
	
	}else{
		return -3; //File Exists
	}
	return 0;
}

int copyFile(char* sourceFile, char* sourceBuffer, char* destinyFile){
	int fileSize; 
	readFile(sourceFile, sourceBuffer);
	fileSize = getFileSize(sourceFile);

	return writeFile(destinyFile, sourceBuffer, fileSize*512);
}

int getFileSize(char* filename){
	int fileIdx, j, size = 0;
	char dir[512];

	readSector(dir, 2);
	fileIdx = fileExists(filename, dir);
	if (fileIdx != -1){
		
		for(j = 0; j < 26; j++){
			if(dir[fileIdx+j] !=0x00)
				size++;
			else
				break;
		}
	}
	return size;
}

void itoa(int number, int color){
	int i = 0;	
	char array[10];	
	cleanBuffer(array, 10);
	
	while(number!=0){
		array[i] = (char)mod(number, 10)+48;
		number /= 10;
		i++;	
	}
	
	for(i = 9; i != -1; i--)
		if(array[i]!=0)
			printColoredChar(array[i], color);
}

int strLen(char* buffer){
	int size = 0;
	while(buffer[size] != 0)
		size++;

	return size;
}

int readString2(char* str, int color){
	int flag, i = 0;
	char current = ' ';
	cleanBuffer(str, lineLength);
	while( current != 0x0 ){
		current = readChar();
		if(flag && current == 0xD)
			break;

		if(current == 0xD){
			str[i] = '\n';
			i++;
			printChar('\n', color);
			flag = 1;			
		}else if(current == 0x8 && i > 0){
			printChar(current, 0x7);
			i--;	
			flag = 0;
		}else if(i < lineLength-1 && current != 0x8 && current != 0xD){
			str[i] = current;
			printChar(str[i], color);
			i++;
			flag = 0;
		}
	}

	str[i] = 0;
	return i;
}

void scheduleProcess(){
	
	int i, formerCurrentProcessIndex, found = 0;
	setKernelDataSegment();
	i = formerCurrentProcessIndex = getCurrentProcessIndex();
	i++;
	for( ; !found; i++){	
		if(i==8){ i = 0; }

		if(i == formerCurrentProcessIndex)
			break;

		if(process_queue[i].status == 1){
			if(currentProcess!=0 && currentProcess->status==3)
				currentProcess->status =1;

			//process_queue[i].status = 3;
			//process_queue[formerCurrentProcessIndex].status = 1;
			currentProcess = &process_queue[i];
			currentProcess->status =3;
			found = 1; 
		}
	
		
		
	}
	restoreDataSegment();
}

int getCurrentProcessIndex(){
	int i = 0;

	for(; i < 8; i++)
		if(currentProcess->segment == process_queue[i].segment)
			return i;

	return -1;
}

void listProcess(){
	int i = 0;	
	setKernelDataSegment();
	printColoredString("No.     ", 0xD); printColoredString("status     ", 0xD); printColoredString("segment  ", 0xD); 
	
	for(; i < 8; i++){
				
		printColoredString("P", 0xF); itoa(i,0xF); printString("               ");
		switch(copy_process_queue[i].status ){
			case 1: printColoredString("READY"); break;
			case 2: printColoredString("WAITING"); break;			
			case 3: printColoredString("RUNNING"); break;			
			case 4: printColoredString("DEAD"); break;						
					
		}
		itoa(copy_process_queue[i].segment);
	}	

	restoreDataSegment();
}

void copyProcessQueue(){
	int i = 0;
	for(; i < 8; i++){
		copy_process_queue[i] = process_queue[i];
	}
}

void Kill(int index){
	setKernelDataSegment();
	if(index > 0 && index < 8)	
		process_queue[index].status = 4;
	restoreDataSegment();
}

