;kernel.asm contains assembly functions that you can use in your kernel
.data
	.extern _currentProcess 
.text
	.global _putInMemory
	.global _makeInterrupt21
	.global _loadProgram
	.global _readChar
	.global _readSector
	.global _interrupt21ServiceRoutine
	.extern _printString
	.extern _printColoredString
	.extern _readString
	.extern _readFile
	.global _clrScreen
	.global _launchProgram
	.global _copyToSegment
	.extern _execute
	.extern _listFiles
	.global _printColoredChar
	.global _moveCursor
	.global _getCurrentRow
	.global _getCurrentColumn
	.extern _terminate
	.global _scrollUp
	.extern _drawBar
	.global _mod
	.extern _deleteFile
	.global _writeSector
	.extern _writeFile
	.extern _copyFile
	.extern _readString2
	.global _timerISR
	.global _irqInstallHandler
	.global _setTimerPhase
	.global _setKernelDataSegment
	.global _restoreDataSegment
	.global _printhex
	.extern _scheduleProcess

;void putInMemory (int segment, int address, char character)
_putInMemory:
	push bp
	mov bp,sp
	push si
	push ds
	mov ax,[bp+4]
	mov si,[bp+6]
	mov cl,[bp+8]
	mov ds,ax
	mov [si],cl
	pop ds
	pop si
	pop bp
	ret
;_mod(dividend, divisor)
_mod:
	push bp
	mov bp, sp
	xor dx, dx
	mov ax, [bp+4]
	mov bx, [bp+6]
	div bx
	mov ax, dx
	pop bp
	ret

_printColoredChar:
	push bp
	mov bp,sp
	mov ah,#9 
	mov al,[bp+4]   
	mov bh,#0  
	mov bl,[bp+6]  
	mov cx,#1
	int #0x10  
	mov ah,#0x3
	mov bh,#0
	int #0x10		;get cursor position
	add dl,#1	;add column
	mov ah,#0x2
	int #0x10
	pop bp
	ret

_readChar:
	mov ah, #0
	int #0x16
	ret

_loadProgram:
	mov ax, #0x2000
	mov ds, ax
	mov ss, ax
	mov es, ax 
	mov ax, #0xfff0 	;let's have the stack start at 0x2000:fff0
	mov sp, ax
	mov bp, ax		; Read the program from the floppy
	mov	cl, #12		;cl holds sector number
	mov	dh, #0		;dh holds head number - 0
	mov	ch, #0		;ch holds track number - 0
	mov	ah, #2		;absolute disk read
	mov	al, #1		;read 1 sector
	mov	dl, #0		;read from floppy disk A
	mov	bx, #0		;read into offset 0 (in the segment)
	int #0x13 		;call BIOS disk read function
	jmp #0x2000:#0 	; Switch to program

;_readSector(char *buffer, int sector)
_readSector:
	push bp
	mov bp, sp
	sub sp, #6
	mov bx, [bp+4]
	mov ax, [bp+6]
	mov cl, #36
	div cl
	xor ah, ah
	mov [bp-2], ax
	mov ax, [bp+6]
	mov cl, #18
	div cl
	and al, #0x1
	xor dx, dx
	mov dl, al
	mov [bp-4], dx
	inc ah
	xor dx, dx
	mov dl, ah
	mov [bp-6], dx
	mov ah, #0x2 	;Read Sector
	mov al, #0x1
	mov ch, [bp-2]
	mov cl, [bp-6]
	mov dh, [bp-4]
	mov dl, #0 		;End Read Sector
	int #0x13
	add sp, #6
	pop bp
	ret

;_writeSector(char *buffer, int sector)
_writeSector:
	push bp
	mov bp, sp
	sub sp, #6
	mov bx, [bp+4]
	mov ax, [bp+6]
	mov cl, #36
	div cl
	xor ah, ah
	mov [bp-2], ax
	mov ax, [bp+6]
	mov cl, #18
	div cl
	and al, #0x1
	xor dx, dx
	mov dl, al
	mov [bp-4], dx
	inc ah
	xor dx, dx
	mov dl, ah
	mov [bp-6], dx
	mov ah, #0x3 	;Write Sector
	mov al, #0x1
	mov ch, [bp-2]
	mov cl, [bp-6]
	mov dh, [bp-4]
	mov dl, #0 		;End Write Sector
	int #0x13
	add sp, #6
	pop bp
	ret

_clrScreen:
	mov ah,#6    
        mov al,#0
        mov bh,#7
        mov cx,#0
        mov dl,#79
        mov dh,#24
        int  #0x10
	call _drawBar
	call _setCursor
	ret

_setCursor:
	mov ah, #2		;Set cursor position
	mov bh, #0		;Page Number
	mov dh, #3		;Row
	mov dl, #0		;Column
	int #0x10
	mov ah, #0xB		;Set background/border Color
	mov bh, #0		
	mov bl, #0xC0   	;Set color
	int #0x10
	ret

_moveCursor:
	push bp
	mov bp,sp
	mov bh, #0		;Page Number
	mov dh,[bp+6]   	;Row
	mov dl,[bp+4]   	;Column
	mov ah,#0x2
	int #0x10
	pop bp
	ret

_getCurrentRow:
	mov ah,#0x3
	mov bh,#0  ;  page
	int #0x10
	xor ax,ax
	mov al,dh
	ret

_getCurrentColumn:
	mov ah,#0x3
	mov bh,#0  ;  page
	int #0x10
	xor ax,ax
	mov al,dl
	ret

;void makeInterrupt21()
;this sets up the interrupt 0x21 vector
;when an interrupt 0x21 is called in the future, 
;_interrupt21ServiceRoutine will run
_makeInterrupt21:
	mov dx,#_interrupt21ServiceRoutine	;get the address of the service routine
	push ds
	mov ax, #0	;interrupts are in lowest memory
	mov ds,ax
	mov si,#0x84	;interrupt 0x21 vector (21 * 4 = 84)
	mov ax,cs	;have interrupt go to the current segment
	mov [si+2],ax
	mov [si],dx	;set up our vector
	pop ds
	ret

_ps:
	push bx
	call _printString
	add sp, #2
	jmp _end

_rs:
	push cx
	push bx
	call _readString
	add sp, #4
	jmp _end

_rsec:
	push cx
	push bx
	call _readSector
	add sp, #4
	jmp _end

_file:
	push cx
	push bx
	call _readFile
	add sp, #4
	jmp _end

_clr: 
	call _clrScreen
	jmp _end

_exeprg:
	push bx
	call _execute
	add sp, #2
	jmp _end

_mc:
	push cx
	push bx
	call _moveCursor
	add sp, #4
	jmp _end

_ls:
	call _listFiles
	jmp _end

_pcs:
	push cx
	push bx
	call _printColoredString
	add sp, #4
	jmp _end

_terminateProgram:
	call _terminate
	jmp _end
_dF:
	push bx
	call _deleteFile
	add sp, #2
	jmp _end

_ws:
	push cx
	push bx
	call _writeSector
	add sp, #4
	jmp _end

_end:
	iret
	
;this is called when interrupt 21 happens
;it will call your function:
;void handleInterrupt21 (int AX, int BX, int CX, int DX)
_interrupt21ServiceRoutine:
	cmp ax, #0
	je _ps
	cmp ax, #1
	je _rs
	cmp ax, #2
	je _rsec
	cmp ax, #3
	je _file
	cmp ax, #4
	je _clr
	cmp ax, #5
	je _exeprg
	cmp ax, #6
	je _mc
	cmp ax, #7
	je _ls
	cmp ax, #8
	je _pcs
	cmp ax, #9
	je _terminateProgram
	cmp ax, #10
	je _dF
	cmp ax, #11
	je _wF
	cmp ax, #12
	je _copyF
	cmp ax, #13
	je _rs2
	jmp _end

_wF:
	push dx
	push cx
	push bx
	call _writeFile
	add sp, #6
	jmp _end

_copyF:
	push dx
	push cx
	push bx
	call _copyFile
	add sp, #6
	jmp _end

_rs2:
	push cx
	push bx
	call _readString2
	add sp, #4
	jmp _end

;this is called to start a program that is loaded into memory
;void launchProgram(int segment)
_launchProgram:
	mov bp,sp
	mov bx,[bp+2]	;get the segment into bx
	mov ds, bx
	mov ss, bx	
	mov es, bx
	mov ax, #0xfff0 	
	mov sp, ax
	mov bp, ax
	push bx
	push #0
	retf

_copyToSegment:
	push bp
	mov bp, sp
	push si
	push di
	mov es, [bp+4]	;segment
	mov di, [bp+6]	;destiny Address
	mov si, [bp+8]	;source Address
	mov cx, [bp+10]	;size
	rep
	movsb
	pop di
	pop si
	pop bp
	ret

_scrollUp:
	push bp
	mov bp, sp
	mov ah, #0x6
	mov al, [bp+4]		;# of lines to scroll
	mov bh, #0x7
	xor cx, cx
	mov dh, #24
	mov dl, #79
	int #0x10
	pop bp
	ret

; void irqInstallHandler(int irq_number, void (*fn)())
; Install an IRQ handler
_irqInstallHandler:
	cli 
	push bp
	mov bp,sp
	
	push si
	push ds
	mov dx, #_timerISR;function pointer
	xor ax,ax
	mov ds,ax             ;Interrupt vextor is at lowest
	mov si,#0x8
	shl si, #2               ;ax=irq_handler *4
	
	mov ax, cs
	mov [si+2], ax
	mov [si] , dx
	
	pop ds
	pop si
	pop bp
	
	sti
	ret
; void setTimerPhase(int hz)
; Set the timer frequency in Hertz
_setTimerPhase:
	push bp
	mov bp, sp
	mov dx, #0x0012 ; Default frequency of the timer is 1,193,180 Hz
	mov ax, #0x34DC
	mov bx, [bp+4]
	div bx
	
	mov bx, ax	; Save quotient
	
	mov dx, #0x43
	mov al, #0x36
	out dx, al	; Set our command byte 0x36
	
	mov dx, #0x40
	mov al, bl
	out dx, al	; Set low byte of divisor
	mov al, bh
	out dx, al	; Set high byte of divisor

	pop bp
	ret
; void setKernelDataSegment()
; Sets the data segment to the kernel, saving the current ds on the stack
_setKernelDataSegment:
	pop bx
	push ds
	push bx
	mov ax, #0x1000
	mov ds, ax
	ret
; void restoreDataSegment()
; Restores the data segment
_restoreDataSegment:
	pop bx
	pop ds
	push bx
	ret

;this routine runs on timer interrupts
_timerISR:
        ;disable interrupts
        cli
        ;save all regs for the old process on the old process's stack
        push bx
        push cx
        push dx
        push si
        push di
        push bp
        push ax
        push ds
        push es

        ;reset interrupt controller so it performs more interrupts
        mov al, #0x20
        out #0x20, al
	
	mov cx , sp

        ;set all segments to the kernel
        mov ax, #0x1000
        mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, #0xff00

	mov bx, [_currentProcess]
	test bx, bx
	jz gotoSchedule

	mov [bx+2], cx
	
	gotoSchedule:
        call _scheduleProcess
	
	mov bx, [_currentProcess]
	mov sp, [bx+2]
	mov ss, [bx+4]

	pop es
        pop ds
        pop ax
        pop bp
        pop di
        pop si
        pop dx
        pop cx
        pop bx
	sti
	iret

_printhex:
        push bx
        push ax
        push ax
        push ax
        push ax
        mov al,ah
        mov ah,#0xe
        mov bx,#7
        shr al,#4
        and al,#0xf
        cmp al,#0xa
        jb ph1
        add al,#0x7
ph1:    add al,#0x30
        int 0x10

        pop ax
        mov al,ah
        mov ah,#0xe
        and al,#0xf
        cmp al,#0xa
        jb ph2
        add al,#0x7
ph2:    add al,#0x30
        int 0x10

        pop ax
        mov ah,#0xe
        shr al,#4
        and al,#0xf
        cmp al,#0xa
        jb ph3
        add al,#0x7
ph3:    add al,#0x30
        int 0x10

        pop ax
        mov ah,#0xe
        and al,#0xf
        cmp al,#0xa
        jb ph4
        add al,#0x7
ph4:    add al,#0x30
        int 0x10

        pop ax
        pop bx
        ret
