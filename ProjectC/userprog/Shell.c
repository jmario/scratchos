char cmd[20];
char target[7];
char target2[7];
char instruction[26];
void cleanbuffer();
void strtok(char* str);
void nextLine();
int validCommand();
void FNF();
void displayInfo();
void prompt();
int onlySpaces();
void writeFileResult(int writeFileReturnValue);

void main(){
	int i;	
	char buffer[13312];
	
	while(1){
		
		nextLine();	prompt();	cleanbuffer();
		syscall_readString(instruction, 0xF);
		strtok(instruction);
		switch(validCommand()){
			case 0:
				syscall_readFile(target, buffer);
				if(buffer[0] == '\0'){
					FNF();
				}else{
					nextLine();	syscall_printString(buffer);
				}
			break;
			case 1:	
				syscall_readFile(target, buffer);
				if(buffer[0] == '\0')
					FNF();
				else
					syscall_execute(target);
			break;
			case 2:
				syscall_clr();
			break;
			case 3:
				nextLine();	syscall_printColoredString(" Files    Sectors   Size(bytes)  ", 0x4F);
				nextLine();	syscall_printColoredString("------------------------", 0xF);
				nextLine();	syscall_listFiles();
			break;
			case 4:
				displayInfo();
			break;
			case 5: /*DO NOTHING*/ break;
			case 6: 
				if(!syscall_deleteFile(target)){
					FNF();
				}else{
					nextLine();
					syscall_printString("\t\t\t\t\t\t"); 
					syscall_printColoredString("| File ", 0x3F);
					syscall_printColoredString(target, 0x30);
					syscall_printColoredString(" | Deleted Successfully", 0x3F);
					nextLine();
				}
			break;
			case 7:
				i = syscall_copyFile(target, buffer, target2);
				writeFileResult(i);

			break;
			case 8:	
				syscall_printColoredString(" Type your text: ", 0x3F);
				nextLine();
				i = syscall_readString2(buffer, 0x7);
				i = syscall_writeFile(target, buffer, i);
				writeFileResult(i);

			break;
 			default: 
				nextLine(); syscall_printString("\t\t\t\t\t\t"); 
				syscall_printColoredString("|    invalid command    |", 0x5F); nextLine();
		}


	}
}

void strtok(char* str){
	
	int i, j = 0;
	for(i = 0; str[i] != ' ' && str[i] != '\0'; i++){
		cmd[i] = str[i];	
	}
	cmd[i] = '\0';
	i++;

	for(; str[i] != ' ' && str[i] != '\0'; i++, j++){
		target[j] = str[i];
	}
	target[j] = '\0';
	i++;
	if(cmd[0]=='c' && cmd[1]=='o' && cmd[2]=='p' && cmd[3]=='y' && cmd[4]== 0){
		for(j = 0; str[i] != ' ' && str[i] != '\0'; i++, j++){
			target2[j] = str[i];
		}
		target2[j] = '\0';	
	}
}	

void cleanbuffer(){
	int i = 0;	
	for(; i<26; i++){
		if(i < 7){
			target[i] = '\0'; target2[i] = '\0';
		}
		
		if(i < 20)
			cmd[i] = '\0';
		
		instruction[i] = '\0';
	}
}

int validCommand(){
	int flag = 0;

	if(cmd[0]=='t' && cmd[1]=='y' && cmd[2]=='p' && cmd[3]=='e' && cmd[4] == '\0')
		flag = 0;
	else if(cmd[0]=='e' && cmd[1]=='x' && cmd[2]=='e' && cmd[3]=='c' && cmd[4]=='u' && cmd[5]=='t' && cmd[6]=='e' && cmd[7] == '\0')
		flag = 1;
	else if(cmd[0]=='c' && cmd[1]=='l' && cmd[2]=='e' && cmd[3]=='a' && cmd[4]=='r' && cmd[5] == '\0')
		flag = 2;
	else if(cmd[0]=='l' && cmd[1]=='s' && cmd[2]== '\0')	
		flag = 3;
	else if(cmd[0]=='h' && cmd[1]=='e' && cmd[2]=='l' && cmd[3]=='p' && cmd[4]== '\0')	
		flag = 4;
	else if(onlySpaces())
		flag = 5;
	else if(cmd[0]=='d' && cmd[1]=='e' && cmd[2]=='l' && cmd[3]=='e' && cmd[4]=='t' && cmd[5]=='e' && cmd[6] == '\0')
		flag = 6;
	else if(cmd[0]=='c' && cmd[1]=='o' && cmd[2]=='p' && cmd[3]=='y' && cmd[4]== 0)
		flag = 7;
	else if(cmd[0]=='c' && cmd[1]=='r' && cmd[2]=='e' && cmd[3]=='a' && cmd[4]=='t' && cmd[5]=='e' && cmd[6]== 0)
		flag = 8;
	else
		flag = -1;
			
	return flag;
}

void displayInfo(){
	nextLine();	syscall_printColoredString("   CMDs   ", 0xE);  syscall_printColoredString("            Description          ", 0x70); 
	syscall_printColoredString(" ", 0x0);		       		syscall_printColoredString("            Usage          ", 0x70);
	nextLine();	syscall_printColoredString("   type   ", 0x2F); syscall_printString("   Prints a document.");
	syscall_printColoredString("              type", 0xE);		syscall_printColoredString("   <Text File>", 0xC);
	nextLine();	syscall_printColoredString(" execute  ", 0x4F); syscall_printString("   Executes a program.");
	syscall_printColoredString("             execute", 0xE);	syscall_printColoredString("  <Program File>", 0xD);
	nextLine();	syscall_printColoredString("    ls    ", 0x3F); syscall_printString("   Lists all available files.");
	syscall_printColoredString("      ls", 0xE);
	nextLine();	syscall_printColoredString("  clear   ", 0x6F); syscall_printString("   Clears the screen.");
	syscall_printColoredString("              clear", 0xE);
	nextLine();	syscall_printColoredString(" delete   ", 0x1F); syscall_printString("   Deletes a File.     ");
	syscall_printColoredString("            delete", 0xE);	syscall_printColoredString("  <Existing File>", 0xA);
	nextLine();	syscall_printColoredString(" copy     ", 0x7F); syscall_printString("   Copies a File to another. ");
	syscall_printColoredString("      copy", 0xE);	syscall_printColoredString("  <Source File> <Destiny File>", 0xA);	
	nextLine();	syscall_printColoredString(" create   ", 0x5F); syscall_printString("   Creates a new File. ");
	syscall_printColoredString("           create", 0xE);	syscall_printColoredString("  <Filename> ::input_text", 0x5);	
	nextLine();
}

void nextLine(){
	syscall_printString("\r\n");
}

void FNF(){
	syscall_printString("\r\n\t\t\t\t\t\t");
	syscall_printColoredString("[error:404] File not found", 0x4E);
	nextLine();
}

void prompt(){
	syscall_printColoredString("|jmibarrad|", 0x3);
	syscall_printColoredString(" $/:",0xF);
}

int onlySpaces(){
	int i=0;	
	for(; i<26; i++)
		if(instruction[i] != ' ' && instruction[i] != '\0')
			return 0;
	return 1;		
}

void writeFileResult(int writeFileReturnValue){
	if(writeFileReturnValue == 0){
		syscall_printColoredString(" | Create Successful |", 0x2F);	
	}else if(writeFileReturnValue == -1){
		syscall_printColoredString(" | Ran out of Sectors |", 0x4F);
	}else if(writeFileReturnValue == -2){
		syscall_printColoredString(" | Ran out of Slots |", 0x4F);
	}else if(writeFileReturnValue == -3){
		syscall_printColoredString(" | File already Exists |", 0x4F);
	}
}
