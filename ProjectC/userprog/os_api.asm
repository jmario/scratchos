	.global _syscall_printString
	.global _syscall_readString
	.global _syscall_readSector
	.global _syscall_readFile
	.global _syscall_clr
	.global _syscall_execute
	.global _syscall_listFiles
	.global _syscall_printColoredString
	.global _syscall_moveCursor
	.global _syscall_terminate
	.global _syscall_deleteFile
	.global _syscall_writeFile
	.global _syscall_copyFile
	.global _syscall_readString2

_syscall_printString:
	push bp
	mov bp, sp
	mov ax, #0
	mov bx, [bp+4]
	int #0x21
	pop bp
	ret
	
_syscall_readString:
	push bp
	mov bp, sp
	mov ax, #1
	mov bx, [bp+4]
	mov cx, [bp+6]
	int #0x21
	pop bp
	ret
	
_syscall_readSector:
	push bp
	mov bp, sp
	mov ax, #2
	mov bx, [bp+4]
	mov cx, [bp+6]
	int #0x21
	pop bp
	ret

_syscall_readFile:
	push bp
	mov bp, sp
	mov ax, #3
	mov bx, [bp+4]
	mov cx, [bp+6]
	int #0x21
	pop bp
	ret

_syscall_clr:
	mov ax, #4
	int #0x21
	ret

_syscall_execute:
	push bp
	mov bp, sp
	mov ax, #5
	mov bx, [bp+4]
	int #0x21
	pop bp
	ret

_syscall_moveCursor:
	push bp
	mov bp, sp
	mov ax, #0x6
	mov dh, [bp+4]	;row
	mov dl, [bp+6]	;column
	int #0x21
	pop bp
	ret

_syscall_listFiles:
	mov ax, #7
	int #0x21
	ret

_syscall_printColoredString:
	push bp
	mov bp, sp
	mov ax, #8
	mov bx, [bp+4]
	mov cx, [bp+6]
	int #0x21
	pop bp
	ret

_syscall_terminate:
	mov ax, #9
	int #0x21
	ret

_syscall_deleteFile:
	push bp
	mov bp, sp
	mov ax, #10
	mov bx, [bp+4]
	int #0x21
	pop bp
	ret

_syscall_writeFile:
	push bp
	mov bp, sp
	mov ax, #11
	mov bx, [bp+4]
	mov cx, [bp+6]
	mov dx, [bp+8]
	int #0x21
	pop bp
	ret
_syscall_copyFile:
	push bp
	mov bp, sp
	mov ax, #12
	mov bx, [bp+4]
	mov cx, [bp+6]
	mov dx, [bp+8]
	int #0x21
	pop bp
	ret

_syscall_readString2:
		push bp
	mov bp, sp
	mov ax, #13
	mov bx, [bp+4]
	mov cx, [bp+6]
	int #0x21
	pop bp
	ret
