void getResponse(char resp, char resp2);
void displayScore();
char ans[1];
int score = 0;

void main(){

	syscall_printColoredString("\n\t\t\t\t\t\t     ASSEMBLY TRIVIA:  ", 0xB);
	syscall_printColoredString("MIPS32     ", 12);
	syscall_printColoredString("\nAnswer (T)True or (F)False: \n", 0xE);
	
	syscall_printColoredString("\r\n1)", 0xA);
	syscall_printColoredString(" Is MIPS a CISC instruction set?  ", 0xF);	
	syscall_readString(ans, 12);
	getResponse('F', 'f');

	syscall_printColoredString("\r\n2)", 0xA);
	syscall_printColoredString(" Are MFHI and MFLO R-type instructions?  ", 0xF);	
	syscall_readString(ans, 12);
	getResponse('T', 't');

	syscall_printColoredString("\r\n3)", 0xA);
	syscall_printColoredString(" If MIPS had 64 registers, you would need 6 bits to specify the register?  ", 0xF);	
	syscall_readString(ans, 12);
	getResponse('T', 't');

	syscall_printColoredString("\r\n4)", 0xA);
	syscall_printColoredString(" Does J-type instruction 'jr' stands for 'junior'?  ", 0xF);	
	syscall_readString(ans, 12);
	getResponse('F', 'f');

	syscall_printColoredString("\r\n5)", 0xA);
	syscall_printColoredString(" Callee must preserve registers $a0-$a7?  ", 0xF);	
	syscall_readString(ans, 12);
	getResponse('F', 'f');	
	displayScore();
	syscall_terminate();
}

void getResponse(char resp, char resp2){
	if(ans[0] == resp || ans[0] == resp2){
		syscall_printString("\n\t\t\t\t\t\t");
		syscall_printColoredString(" Correct Answer \n", 0x2F);
		score++;
	}else{
		syscall_printString("\n\t\t\t\t\t\t");
		syscall_printColoredString(" Wrong Answer \n", 0x4F);
	}
}

void displayScore(){
	syscall_printColoredString("\n\n\t\t\t\t Score: ", 0x89);
	if(score==0)
		syscall_printString("0");
	else if(score==1)
		syscall_printString("1");
	else if(score==2)
		syscall_printString("2");
	else if(score==3)
		syscall_printString("3");
	else if(score==4)
		syscall_printString("4");
	else if(score==5)
		syscall_printString("5");

	syscall_printString("/5");

	if(score==5)
		syscall_printColoredString("\t\t\t\t\t\t   Flawless Victory  ", 0xBF);
	syscall_printString("\n");
}


